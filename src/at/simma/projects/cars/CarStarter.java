package at.simma.projects.cars;

public class CarStarter {

	public static void main(String[] args) {
		Producer p1 = new Producer("VW", "Germany", 25);
		Engine e1 = new Engine("Benzin", 100);
		Car c1 = new Car(100, 20000, 5, p1, e1);
		Car c2 = new Car(200, 40000, 10, p1, e1);
		Person pe1 = new Person("Max", "Mustermann", "01.01.2000");
		pe1.addCar(c1);
		pe1.addCar(c2);
		System.out.println("Das Auto vom Hersteller " + c1.getProducerName() + " kostet " + c1.getPrice()
				+ " � und ben�tigt f�r eine Strecke von 10000km " + c1.getUsage(10000) + "l " + c1.getType() + ".");
		System.out.println("Die Person " + pe1.getFirstName() + " " + pe1.getLastName() + " besitzt Autos im Wert von "
				+ pe1.getValueOfCars() + "�.");
		for (int i = 0; i < 10; i++) {
			System.out.println((double)(2*1+7)/(5*i+1));
		}
	}

}
