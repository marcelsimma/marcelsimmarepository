package at.simma.projects.cars;

public class Car {
	private double maxSpeed, basePrice, basicUsage;
	// km/h � l/100km
	Producer producer;
	Engine engine;

	public Car(double maSp, double baPr, double baUs, Producer pr, Engine en) {
		maxSpeed = maSp;
		basePrice = baPr;
		basicUsage = baUs;
		producer = pr;
		engine = en;
	}

	public double getPrice() {
		return this.basePrice * (100 - this.producer.getDiscount()) / 100;
	}

	public String getType() {
		return this.engine.getFuel();
	}

	public double getUsage(double way) {
		if (way > 50000) {
			return ((way - 50000) * 1.098 + 50000) * this.basicUsage/* l/100km */ / 100/* km */;
		} else {
			return way * this.basicUsage/* l/km */ / 100/* km */;
		}
	}

	public String getProducerName() {
		return this.producer.getName();
	}

}
