package at.simma.projects.cars;

import java.util.ArrayList;
import java.util.Calendar;

public class Person {
	private String firstName, lastName;
	private ArrayList<Car> cars;

	public ArrayList<Car> getCars() {
		return cars;
	}

	public void setCars(ArrayList<Car> cars) {
		this.cars = cars;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	private Calendar birthday;
	// private String birth;

	public Person(String fiNa, String laNa, String birt) {
		firstName = fiNa;
		lastName = laNa;
		cars = new ArrayList<>();
		// birth = birt;

		/*
		 * String[] parts = this.birth.split("."); birthday = Calendar.getInstance ();
		 * String year = parts[2]; String month = parts[1]; String day = parts[0];
		 * System.out.println(year); //birthday.set(year, month, day );
		 * System.out.println(birthday.getTime());
		 */
	}

	void addCar(Car c) {
		cars.add(c);
	}

	double getValueOfCars() {
		double carSum = 0;
		for (Car car : cars) {
			carSum += car.getPrice();
		}
		return carSum;
	}
}
