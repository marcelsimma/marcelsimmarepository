package at.simma.projects.cars;

public class Producer {
	private String name, countryOfOrigin;
	private double discount; // in Prozent

	public Producer(String na, String coOfOr, double di) {
		name = na;
		countryOfOrigin = coOfOr;
		discount = di;
	}

	public double getDiscount() {
		return discount;
	}

	public String getName() {
		return name;
	}
	
}