package at.simma.projects.cars;

public class Engine {
	private String fuel;
	private double power; // in PS

	public Engine(String fu, double po) {
		fuel = fu;
		power = po;
	}

	public String getFuel() {
		return fuel;
	}
}